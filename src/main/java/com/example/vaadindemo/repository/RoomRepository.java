package com.example.vaadindemo.repository;

import com.example.vaadindemo.entity.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class RoomRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Room> findAll() throws Exception {
        return em.createNamedQuery(Room.FIND_ALL).getResultList();
    }

    public void save(Room room) throws  Exception {
        em.persist(room);
    }

    public void delete(Long id) throws Exception{
        em.remove(findById(id));

    }

    public void update(Room room) throws Exception{
        em.merge(room);
    }

    public Room findById(Long id) throws Exception{
        return em.find(Room.class, id);
    }
}
