package com.example.vaadindemo.repository;


import com.example.vaadindemo.entity.Course;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class CourseRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Course> findAllByRoomId(Long id) throws Exception {
        Query query=em.createNamedQuery(Course.FIND_BY_ROOM_ID);
        query.setParameter("roomId",id);
        return query.getResultList();
    }

    public List<Course> findAll() throws Exception {
        return em.createNamedQuery(Course.FIND_ALL).getResultList();
    }

    public void save(Course course) throws  Exception {
        em.persist(course);
    }

    public void delete(Long id) throws Exception{
        em.remove(findById(id));

    }

    public void update(Course course) throws Exception{
        em.merge(course);
    }

    public Course findById(Long id) throws Exception{
        return em.find(Course.class, id);
    }
}
