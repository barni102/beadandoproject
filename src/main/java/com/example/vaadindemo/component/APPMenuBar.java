package com.example.vaadindemo.component;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class APPMenuBar extends HorizontalLayout {

    public APPMenuBar(){
        buildAChor("Main","/");
        buildAChor("Courses","/courses");
        buildAChor("Rooms","/rooms");
    }
    private void buildAChor(String text,String href){
        Anchor anchor=new Anchor();
        anchor.setText(text);
        anchor.setHref(href);
        add(anchor);
    }
}
