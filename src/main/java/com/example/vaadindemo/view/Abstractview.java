package com.example.vaadindemo.view;

import com.example.vaadindemo.component.APPMenuBar;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public abstract class Abstractview extends VerticalLayout {
    public void initView(){
        add(new APPMenuBar());
    }
}
