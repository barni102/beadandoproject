package com.example.vaadindemo.view;

public interface RefreshAware {
    void processRefresh();
}
